const express = require("express");
const app = express();
const server = require("http").Server(app);
require("./webSocket")(server);

app.use(express.static("www"));

server.listen(4041);
