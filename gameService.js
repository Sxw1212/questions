var r = require("./db");

var rooms = {};

module.exports = function(socket, room) {
  var userID;
  var currentRoom;
  const intervalID = setInterval(() => {
    userID ? r.table("users").get(userID).update({
      ping: r.now()
    }).run() : null;
  }, 1000 * 60);

  function errorHandler() {
    socket.disconnect();
  }

  socket.on("disconnect", () => {
    userID ? r.removeUser(userID) : null;
    clearInterval(intervalID);
  });

  r.joinRoom(room).then(function(newUserID) {
    userID = newUserID;
    r.roomChangefeed(room).then(cursor => {
      cursor.each(function(err, data) {
        if (err || !data.new_val) {
          errorHandler();
          return;
        }

        currentRoom = data.new_val;
        sendGameData(data.new_val);

      });
    }).catch(errorHandler);

    socket.on("question:update", function(question) {
      r.table("rooms").get(currentRoom.id).update({
        currentQuestion: question
      }).run();
    });

    socket.on("questiontype:update", function(questionType) {
      r.table("rooms").get(currentRoom.id).update({
        currentQuestionType: questionType
      }).run();
    });

    socket.on("question:submit", function() {
      r.table("rooms").get(currentRoom.id).update({
        noSpectate: null,
        waitingOn: currentRoom.noSpectate,
        status: "answer"
      }).run();
    });

    socket.on("answer:update", function(answer) {
      r.table("rooms").get(currentRoom.id).update({
        currentAnswer: answer
      }).run();
    });

    socket.on("answer:submit", function() {
      r.table("rooms").get(currentRoom.id).update({
        noSpectate: null,
        waitingOn: null,
        status: "waiting",
        lastTick: r.now()
      }).run();
    });

    socket.emit("updateGameState", {
      showGameWaitBox: true
    });
  });

  function sendGameData() {
    var gameState = {};

    if (currentRoom.waitingOn == userID) {
      if (currentRoom.status == "question") {
        gameState.showQuestionBox = true;
      } else if (currentRoom.status == "answer") {
        gameState.showAnswerBox = true;
      } else {
        gameState.showSpectateBox = true;
      }
    } else if (currentRoom.noSpectate == userID) {
      gameState.showYouAreUpBox = true;
    } else {
      gameState.showSpectateBox = true;
    }

    gameState.currentAnswer = currentRoom.currentAnswer;
    gameState.currentQuestion = currentRoom.currentQuestion;
    gameState.currentQuestionType = currentRoom.currentQuestionType;
    gameState.players = currentRoom.players;

    socket.emit("updateGameState", gameState);
  }

};
