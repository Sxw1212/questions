const gameService = require("./gameService");

module.exports = function(httpServer) {
  const io = require("socket.io")(httpServer);

  io.on("connection", socket => {

    socket.emit("updateGameState", {
      showJoinBox: true
    });

    socket.once("join", room => {
      gameService(socket, room);
    });
  });
};
