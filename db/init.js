var cfg = {
  db: "questions"
};

function createDB(r) {
    return r.dbList().contains(cfg.db).do(databaseExists => {
        return r.branch(
            databaseExists,
            true,
            r.dbCreate(cfg.db)
        );
    }).run().then(() => {
        return r.db(cfg.db).wait();
    });
}

function createTable(r, name, durability) {
    return r.db(cfg.db).tableList().contains(name).do(tableExists => {
        return r.branch(
            tableExists,
            true,
            r.db(cfg.db).tableCreate(name, {
                durability: durability
            })
        );
    }).run().then(() => {
        return r.db(cfg.db).table(name).wait();
    });
}

module.exports = r => {
    return createDB(r).then(() => {
        return Promise.all([
            createTable(r, "users"),
            createTable(r, "rooms")
        ]);
    });
};
