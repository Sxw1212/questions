var r = require("rethinkdbdash")({
  servers: [{host:"192.168.100.2"}, {host:"192.168.100.5"}],
  db: "questions"
});

require("./init")(r);

r.joinRoom = room => {
  return r.checkRoom(room).then(function() {
    return r.table("users").insert({
      room: room,
      ping: r.now()
    }).run().then(res => {
      return res.generated_keys[0];
    });
  });
};

r.checkRoom = room => {
  return r.branch(r.table("rooms").filter({room: room}).isEmpty(), r.table("rooms").insert({
    room: room,
    lastTick: r.now(),
    status: "waiting"
  }), {created: 0});
};

r.roomChangefeed = room => {
  return r.table("rooms").filter({room: room}).changes().run();
};

r.removeUser = userID => {
  return r.table("users").filter({id: userID}).delete();
};

module.exports = r;

require("./worker");
