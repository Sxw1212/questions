const r = require("./");

setInterval(function() {
  r.table("users").filter(r.row("ping").lt(r.now().sub(60 * 2))).delete();

  r.table("rooms").filter(r.row("lastTick").lt(r.now().sub(5))).run().then(rooms => {
    rooms.forEach(room => {
      var doUpdate = {};
      doUpdate.lastTick = r.now();
      r.table("users").filter({room: room.room}).count().run().then(players => {
        doUpdate.players = players;
      }).then(() => {
        if (doUpdate.players > 1 && room.status == "waiting") {
          doUpdate.status = "question";
          return r.table("users").filter({room: room.room}).sample(2).then(user => {
            doUpdate.waitingOn = user[0].id;
            doUpdate.noSpectate = user[1].id;
            doUpdate.currentAnswer = "";
            doUpdate.currentQuestion = "";
            doUpdate.currentQuestionType = "";
          });
        }
      }).then(() => {
        if (doUpdate.players > 0) {
          return r.table("rooms").get(room.id).update(doUpdate).run();
        } else {
          return r.table("rooms").get(room.id).delete().run()
        }
      });
    });
  });
  console.log("Worker running");
}, 1000 * 5);
